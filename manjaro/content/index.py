team = [
    {
    "id": "editions",
    "title": "Diferent UI's to choose from",
    "description": `We have a desktop enviroment ( user interface flavor ) for everyone,
                known as editions. There are two types of editions, Official and Community.
                Official are strictly maintained by the 
                <span class="branding-font bold">manjaro</span> team while
                community could be maintained by team members or a community user. 
                A edition might also serve a purpose, like for example 
                <span class="highlight">Architect</span>, allowing you to customize
                your OS from the ground up.`
    },
]