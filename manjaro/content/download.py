descriptions = {
    "x86": "Intel or AMD 64-bit desktops, laptops with legacy Bios or UEFI.",
    
    "arm": "Aarch64 based devices, laptops and phones.",
    
    "plasma": """Plasma Desktop is the most complete in terms of configuration,
                with many configuration options available in the UI, the user 
                interface somehow resembles Windows interface.""",
                
        "xfce": """Xfce Desktop aims to be fast and lightweight whithout reducing features,
                   the user interface resembles Windows XP series.""",
                   
        "gnome": """Gnome Desktop has a modern approach to desktop computing and
                    it is built to also work on tablets out of box.""",
                    
        "openbox": "Openbox is a highly configurable window manager.",
        
        "deepin": "",
        
        "cinnamon": "Cinnamon Is another alternative desktop forked from Gnome 3 and somehow also resembles windows UI.",
        
        "lxqt": "LXQt is a lightweight desktop environment. It is a merge of the LXDE and Razor-qt.",
        
        "lxde": """Lxde is desktop environment with comparatively very low resource requirements.
                   Fully functional but it is now discontinued.""",
        
        "budgie": "Budgie desktop is a alternative desktop experience that integrates with Gnome applications and technologies.",
        
        "i3": "i3 is a dynamic tiling window manager.",
        
        "mate": "Mate is another alternative desktop that was forked from Gnome 2 and somehow also resembles Windows UI.",
        
        "sway": "Sway is a tiling Wayland compositor and a drop-in replacement for the i3 window manager."
    }
