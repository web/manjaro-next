team = [
    {
    "name": "Philip Müller",
    "title": "Lead Developer",
    "description": "Official editions, kernels, software packaging, build/dev tools, marketing, native apps."
    },
    {
    "name": "Bernhard Landauer",
    "title": "Developer",
    "description": "Cinnamon, Deepin, i3, development/build tools, packaging and optimisation within hardware projects."
    },
    {
    "name": "Artem Grinev",
    "title": "Software Developer",
    "description": "Pamac Qt, build server, MSM"
    },
    {
    "name": "Guillaume Benoit",
    "title": "Software Engineer",
    "description": "Pamac Gtk, libpamac, Gnome."
    },
    {
    "name": "Stefano Capitani",
    "title": "Developer/Packager",
    "description": "Gnome, Budgie, GTK Themes, build tools"
    },
    {
    "name": "Frede Hundewadt",
    "title": "DevOps/Engineer",
    "description": "Openbox, lxde, lxqt editions, pacman-mirrors, manjaro-application-utility."
    },
    {
    "name": "Matti Hyttinen",
    "title": "Developer/Packager",
    "description": "Architect, gnome-layout-switcher, Gnome edition, build tools."
    },
    {
    "name": "Dan Johansen",
    "title": "ARM Lead Developer",
    "description": "ARM editions, tools and infrastructure"
    },
    {
    "name": "Josh Crowder",
    "title": "ARM Developer",
    "description": "ARM editions, tools and infrastructure."
    },
    {
    "name": "Vitor Lopes",
    "twitter": "https://twitter.com/codesardine",
    "title": "Web/Software Developer",
    "description": "Web presence/infrastructure, native applications."
    },
    {
    "name": "Helmut Stult",
    "title": "Speaker/Developer",
    "description": "Speaker at Linux Events in Germany and Austria. Packaging, Cinnamon, backup and security solutions."
    },
    {
    "name": "Bogdan Covaciu",
    "title": "Designer/Illustrator",
    "description": "Artwork, themes, testing."
    },
    {
    "name": "Oğuz Kağan Eren",
    "title": "Developer",
    "description": "Web presence/wiki, documentation, native apps."
    },
    {
    "name": "Tobias Mädel",
    "title": "ARM Developer",
    "description": "ARM support, focused on the Pinebook Pro."
    },
    {
    "name": "Tobias Schramm",
    "title": "ARM Kernel Developer",
    "description": "Wrote mainline Linux kernel support for the Pinebook Pro, hardware and electronics debugging, works on ARM device support."
    },
    {
    "name": "Furkan Kardame",
    "title": "ARM Developer",
    "description": "ARM editions/testing, tools and infrastructure."
    },
    {
    "name": "Ray Sherwin",
    "title": "ARM Developer",
    "description": "Raspberry Pi devices/kernels and editions."
    },
    {
    "name": "Mark Wagie",
    "title": "Packager",
    "description": "software packaging, testing, Gnome edition."
    },
    {
    "name": "Jonas Strassel",
    "title": "DevOps/Engineer",
    "description": "Sway edition, infrastructure and software packaging."
    },
    { 
    "name": "Simon Büeler",
    "title": "DevOps/Engineer",
    "description": "Sway edition, infrastructure and software packaging."
    },
    { 
    "name": "Coffee",
    "title": "Motivator",
    "description": "I keep the team sharp, they start working when I do."
    }
    ]
