import requests
from flask import Flask


app = Flask(__name__, instance_relative_config=True)
options = {
    "address": "/download/latest/",
    "redirectCode": 302
}
    
def file_info():
    source = "https://gitlab.manjaro.org/webpage/iso-info/-/raw/master/file-info.json"
    response = requests.get(source)
    return response.json() 

from manjaro import routes

