from flask import Flask, render_template
from manjaro import app, file_info
from manjaro.downloads import editions


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/hardware/")
def hardware():
    return render_template("hardware.html")

@app.route("/download/")
def download():
    from manjaro.content.download import descriptions
    iso_info = file_info()
    return render_template("download.html", iso_info=iso_info, description=descriptions)

@app.route("/news")
def news():
    return render_template("news.html")

@app.route("/team")
def team():
    from manjaro.content.team import team
    return render_template("team.html", content=team)

@app.route("/developer")
def developer():
    return render_template("developer.html")

@app.route("/partners")
def partners():
    return render_template("partners.html")

@app.route("/about")
def tester():
    return render_template("about.html")



