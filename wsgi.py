from manjaro import app

if __name__ == "__main__":
    from livereload import Server
    app.debug = True
    server = Server(app.wsgi_app)
    server.serve()

